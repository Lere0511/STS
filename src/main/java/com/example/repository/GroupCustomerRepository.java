package com.example.repository;

import com.example.domain.GroupCustomer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GroupCustomerRepository extends JpaRepository<GroupCustomer, Integer> {
    @Query("SELECT x FROM GroupCustomer x")
    List<GroupCustomer> findAllOrderByName();

    @Query("SELECT x FROM GroupCustomer x")
    Page<GroupCustomer> findAllOrderByName(Pageable pageable);
}