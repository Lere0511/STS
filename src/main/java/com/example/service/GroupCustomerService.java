package com.example.service;

import com.example.domain.GroupCustomer;
import com.example.repository.GroupCustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GroupCustomerService {
    @Autowired
    GroupCustomerRepository groupCustomerRepository;

    public List<GroupCustomer> findAll() {
        return groupCustomerRepository.findAllOrderByName();
    }

    public Page<GroupCustomer> findAll(Pageable pageable) {
        return groupCustomerRepository.findAllOrderByName(pageable);
    }

    public GroupCustomer findOne(Integer id) {
        return groupCustomerRepository.findOne(id);
    }

    public GroupCustomer create(GroupCustomer groupCustomer) {
        return groupCustomerRepository.save(groupCustomer);
    }

    public GroupCustomer update(GroupCustomer groupCustomer) {
        return groupCustomerRepository.save(groupCustomer);
    }

    public void delete(Integer id) {
        groupCustomerRepository.delete(id);
    }
}