package com.example.web;

import com.example.domain.GroupCustomer;
import com.example.service.GroupCustomerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("groups")	//このGroupCustomerControllerにアクセスするためのパスを指定する	URL
public class GroupCustomerController {
    @Autowired
    GroupCustomerService groupCustomerService;

    @ModelAttribute
    GroupForm setUpForm() {
        return new GroupForm();
    }

    @GetMapping	//何も指定しない場合は@RequestMappingのパスが適用される
    String list(Model model) {
        List<GroupCustomer> groupCustomers = groupCustomerService.findAll();
        //以下2行がどんな動きをしているのかがよくわからん
        model.addAttribute("groups", groupCustomers);	//"customers"はモデル名だって。	え,これどこで使われてんの!?
        return "groups/glist";	//クラスパス。	デフォルトでは.html拡張子がくっつく
        //URLではないけど、パスという解釈に間違いはないと思う。
        //ただ、30の"groups"と31のcustomersはべつもの。
    }

    @PostMapping(path = "create")
    String create(@Validated GroupForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return list(model);
        }
        GroupCustomer groupCustomer = new GroupCustomer();
        BeanUtils.copyProperties(form, groupCustomer);
        groupCustomerService.create(groupCustomer);
        return "redirect:/groups";
    }

    @GetMapping(path = "edit", params = "form")	//URL	変更点
    String editForm(@RequestParam Integer id, GroupForm form) {
        GroupCustomer groupCustomer = groupCustomerService.findOne(id);
        BeanUtils.copyProperties(groupCustomer, form);
        return "groups/gedit";
    }

    //多分更新
    @PostMapping(path = "edit")	//URL	変更点
    String edit(@RequestParam Integer id, @Validated GroupForm form, BindingResult result) {
    	System.out.print(result);
        if (result.hasErrors()) {
            return editForm(id, form);
        }
        GroupCustomer groupCustomer = new GroupCustomer();
        BeanUtils.copyProperties(form, groupCustomer);
        groupCustomer.setId(id);
        groupCustomerService.update(groupCustomer);
        return "redirect:/groups";
    }

    @PostMapping(path = "edit", params = "goToTop")
    String goToTop() {
        return "redirect:/groups";
    }

    @PostMapping(path = "delete")
    String delete(@RequestParam Integer id) {
        groupCustomerService.delete(id);
        return "redirect:/groups";
    }
}