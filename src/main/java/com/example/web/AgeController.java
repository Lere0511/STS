package com.example.web;

import com.example.domain.Customer;
import com.example.service.CustomerService;
import com.example.service.LoginUserDetails;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("age")
public class AgeController {
    @Autowired
    CustomerService customerService;

    @ModelAttribute
    CustomerForm setUpForm() {
        return new CustomerForm();
    }

    @GetMapping(path = "{id}")
    String list(@PathVariable Integer id, Model model) {
       //List<Customer> customers = customerService.findAll();
    	Customer customers = customerService.findOne(id);
        model.addAttribute("ages", customers);	//age以外のデータも入っているんやで
        return "ages/list";
    }

    @GetMapping(path = "edit", params = "form")
    String editForm(@RequestParam Integer id, CustomerForm form) {
        Customer customer = customerService.findOne(id);
        BeanUtils.copyProperties(customer, form);
        return "ages/edit";
    }

    @PostMapping(path = "edit")
    String edit(@RequestParam Integer id, CustomerForm form, BindingResult result,
                @AuthenticationPrincipal LoginUserDetails userDetails) {
    	
    	//ここで現在参照しているname情報が欲しい!!!!!!
    	Customer customers = customerService.findOne(id);
    	String firstName = customers.getFirstName();
    	String lastName = customers.getLastName();
    	System.out.println(firstName +" "+ lastName);
    	
        if (result.hasErrors()) {
        	//そもそもこのエラー自体がformとView側の値の使用方法(多分first_nameとlast_name)があっていないことをさしている。
        	//@Validated	入力値の検証
        	System.out.println("えらー");
            return editForm(id, form);
        }
        Customer customer = new Customer();
        //どっちからどっちにコピーしているのか知りたい
        BeanUtils.copyProperties(form, customer);
        customer.setId(id);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customerService.update(customer, userDetails.getUser());
        return "redirect:/customers";
    }
    /*
    @GetMapping(path = "edit", params = "goToTop")
    String goToTop() {
    	System.out.println("hello");
        return "redirect:/ages";
        //agesのパスでは何も起きないから、ages/id　　とするか   ages以外に飛ばすかした方が良い
    }*/
}